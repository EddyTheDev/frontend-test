## Introduction

We would like you to implement a small exchange-rate website.

It should be possible to enter a price in one currency and get it converted into the selected currency.

The focus should be on writing good and maintainable code. The UI should be usable, but is not the main focus.


## Requirements

- It should both support fiat currencies as well as crypto-currencies.
- It should be possible to exchange between any fiat and any crypto-currency as well.
- The api calls should be cached whenever possible.
- It should be easy to add additional apis in the future.
- It should preferably use Vue.js.

## Details

Additional features and suggestions that didn't make it into the code can be explained and put in a txt file.

For fiat currencies use this api: https://exchangeratesapi.io/.

For crypto-currencies use this api: https://api.binance.com/api/v3/ticker/price (documentation here: https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#symbol-price-ticker)